# -*- coding: utf-8 -*-
import scrapy
import json, re
import copy
from test1.items import Test1Item
from bs4 import BeautifulSoup


class SixsuxSpider(scrapy.Spider):
    name = 'sixsux'
    allowed_domains = ['jd.com', 'p.3.cn']
    start_urls = ['https://list.jd.com/list.html?cat=670%2C671%2C672']

    def parse(self, response):
        # 取出各地分页面链接地址,采用 xpath 路径获取
        brand_urls = response.xpath('//ul[@id="brandsArea"]/li/a/@href').extract()
        brands = response.xpath('//ul[@id="brandsArea"]/li/a/@title').extract()
        # print(len(brand_urls), len(brands))
        for index, brand_url in enumerate(brand_urls):
            brand_url = 'https://list.jd.com' + brand_url
            brand = brands[index]
            print(brand)
            if '华为' in brand:
                brand = 'huawei'
            elif '联想' in brand:
                brand = 'Lenovo'
            elif 'ThinkPad' in brand:
                brand = 'ThinkPad'
            elif 'Apple' in brand:
                brand = 'apple'
            elif '戴尔' in brand:
                brand = 'DELL'
            elif '三星' in brand:
                brand = 'samsung'
            elif '华硕' in brand:
                brand = 'ASUS'
            elif '惠普' in brand:
                brand = 'HP'
            elif '宏碁' in brand:
                brand = 'acer'
            elif '小米' in brand:
                brand = 'xiaomi'
            elif '微软' in brand:
                brand = 'Microsoft'
            elif '外星人' in brand:
                brand = 'Alienware'
            elif '机械革命' in brand:
                brand = 'MECHREVO'
            elif '神舟' in brand:
                brand = 'HASEE'
            elif '微星' in brand:
                brand = 'MSI'
            elif '雷蛇' in brand:
                brand = 'Razer'
            elif '戴睿' in brand:
                brand = 'dere'
            elif '海尔' in brand:
                brand = 'Haier'
            item = Test1Item()
            print(brand)
            item['brand'] = brand
            yield scrapy.Request(url=brand_url, callback=self.parse_list_page, meta={'item': copy.deepcopy(item)})

    def parse_list_page(self, response):
        """
        解析列表页
        :param response:
        :return:
        """
        detail_urls = response.xpath('//li[@class="gl-item"]/div/div[@class="p-img"]/a/@href').extract()
        src_img_urls = response.xpath(
            '//li[@class="gl-item"]/div/div[@class="p-img"]/a[@target="_blank"]/img/@src').extract()
        data_lazy_img_urls = response.xpath(
            '//li[@class="gl-item"]/div/div[@class="p-img"]/a[@target="_blank"]/img/@data-lazy-img').extract()
        # 页面上的图片
        img_urls = src_img_urls + data_lazy_img_urls
        next_url = response.xpath('//a[contains(text(), "下一页")]/@href').extract_first()
        item = response.meta['item']
        if next_url:
            next_url = 'https://list.jd.com' + next_url
            yield scrapy.Request(url=next_url, callback=self.parse_list_page, meta={'item': copy.deepcopy(item)})
        #print(len(detail_urls), len(img_urls))
        for index, detail_url in enumerate(detail_urls):
            detail_url = 'https:' + detail_url
            goods_id = detail_url.split('/')[-1].split('.')[0]
            item['goods_id'] = goods_id
            img_url = 'https:' + img_urls[index]
            item['img_url'] = img_url
            yield scrapy.Request(url=detail_url, callback=self.parse_detail_page, meta={'item': copy.deepcopy(item)})


    def parse_detail_page(self, response):
        """
        解析详情页
        :param response:
        :return:
        """
        title = response.xpath('//title/text()').extract_first()
        title = re.sub('【.*?】|-京东', '', title.strip())
        #print(title)
        item = response.meta['item']
        param = response.xpath('//div[@class="Ptable"]').extract()
        if len(param):
            param = param[0]
        else:
            param = response.xpath('//table[@class="Ptable"]').extract()[0]
        # 处理参数
        param = re.sub('  |\t|\r|\n', '', param)
        soup = BeautifulSoup(param, 'lxml')
        # 对应的参数转为数组
        parmLists=soup.find_all("div", class_="Ptable-item")

        parm_list = []
        for parmList in parmLists:
            # 大标题

            parm_content = {}
            parm_content['bigTitle'] = parmList.h3.string
            #print(parmList.h3.string)
            # 小内容
            small_contants = parmList.find_all("dl", class_="clearfix")
            small_contant_list = []
            for small_contant in small_contants:
                temp = {}
                # 小内容标题
                #print(small_contant.dt.string)
                # 小内容内容
                length = len(small_contant.find_all("dd"))
                #print(small_contant.find_all("dd")[length-1].string)
                temp[small_contant.dt.string] = small_contant.find_all("dd")[length-1].string
                small_contant_list.append(temp)
            parm_content['content'] = small_contant_list
            parm_list.append(parm_content)
        json_str=json.dumps(parm_list, ensure_ascii=False)

        item['title'] = title
        item['param'] = json_str
        price_url = 'https://p.3.cn/prices/mgets?skuIds=J_' + item['goods_id']

        yield scrapy.Request(url=price_url, callback=self.get_goods_price, meta={'item': copy.deepcopy(item)})

    def get_goods_price(self, response):
        """
        获取商品价格
        :param response:
        :return:
        """
        item = response.meta['item']
        json_obj = json.loads(response.text)
        price = json_obj[0]['p']
        item['price'] = price
        rate_url = 'https://club.jd.com/comment/productCommentSummaries.action?referenceIds='+ item['goods_id']
        yield scrapy.Request(url=rate_url, callback=self.get_goods_rate, meta={'item': copy.deepcopy(item)})

    def get_goods_rate(self,response):
        """
        获取商品评价
        :param response:
        :return:
        """
        item = response.meta['item']
        json_obj = json.loads(response.text)
        good_rate = str(json_obj['CommentsCount'][0]['GoodRateShow'])+"%"
        item['good_rate'] = good_rate
        yield copy.deepcopy(item)

