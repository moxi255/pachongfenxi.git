# 爬虫分析

#### 介绍
爬取了京东笔记本信息，并进行分析

#### 软件架构
该软件采用了scrapy框架，进行京东上数据爬取
notebook用来显示


#### 安装教程

1. entrypoint.py文件是运行点，运行这个文件就行了


#### 使用说明

1. entrypoint.py文件运行后，将会进行数据库数据插入
2. notebook用来显示数据


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)